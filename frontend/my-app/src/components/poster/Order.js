import React, {Component} from "react";
import './Post.css'
import {Button, FormControl, InputGroup, Spinner, Table} from "react-bootstrap";
import {getPlaces, getQuestions} from "../../backend/data";
import './Order.css'
import _ from 'lodash'

export default class Order extends Component{
    constructor(props) {
        super(props);
        this.state={
            platforms:[],
            words:'',
        }
    }

    refreshData(){
        getPlaces(this.props.card.id, (res)=>{
            let pl = res.map((item)=>{
                //console.log('item',JSON.parse(item.params))
                return JSON.parse(item.params)
            })
            if (this.props.card.source === 'vk_groups'){
                pl = _.sortBy(pl, (item)=>-this.get_er_vk(item.stats))
            }
            if (this.props.card.source === 'youtube_videos'){
                pl = _.sortBy(pl, (item)=>-this.get_er_youtube(item.stats))
            }

            this.setState({platforms:pl})
        })
    }

    componentDidMount() {
        setInterval(()=>{
            this.refreshData()
        }, 1000)
    }
    get_word(){
        return JSON.parse(this.props.card.params).query
    }
    get_video_link(id){
        return  'https://www.youtube.com/watch?v='+id
    }

    get_group_link(id){
        return  'https://vk.com/'+id
    }

    get_er_youtube(stats){
        console.log(stats)
        return ((parseFloat(stats.likes) + parseFloat(stats.dislikes) + parseFloat(stats.comments))/parseFloat(stats.views)*100).toFixed(2)
    }

    get_er_vk(stats){
        console.log(stats)
        return ((parseFloat(stats.likes) + parseFloat(stats.rep) + parseFloat(stats.comments))/parseFloat(stats.members_count)*100).toFixed(2)
    }

    render() {
        return(
            <div className='order'>
                <div className='order-words'>
                    Ключевые слова: {this.get_word()}
                </div>
                <div className='data'>
                    <Table striped bordered hover>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Название</th>
                            <th>Ссылка</th>
                            <th>ER</th>
                            <th>Просмотров</th>
                        </tr>
                        </thead>
                        <tbody>
                            {this.props.card.source == 'youtube_videos' && this.state.platforms.map((item, i)=>(
                                    <tr>
                                        <td>{i+1}</td>
                                        <td>{item.title}</td>
                                        <td><a href={this.get_video_link(item.video_id)}>Ссылка</a></td>
                                        <td>{this.get_er_youtube(item.stats)}%</td>
                                        <td>{item.stats.views}</td>
                                    </tr>
                            ))}
                            {this.props.card.source == 'vk_groups' && this.state.platforms.map((item, i)=>(
                                <tr>
                                    <td>{i+1}</td>
                                    <td>{item.title}</td>
                                    <td><a href={this.get_group_link(item.id)}>Ссылка</a></td>
                                    <td>{this.get_er_vk(item.stats)}%</td>
                                    <td>{item.stats.members_count}</td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </div>
                <div>
                    <Button variant="success">Выгрузить Exel</Button>
                </div>
            </div>
        )
    }
}