import React, {Component} from "react";
import Post from "./Post";
import {getQuestions, getTasks} from "../../backend/data";
import {Spinner} from "react-bootstrap";
import './Post.css'
import Order from "./Order";

export default class Poster extends Component{
    constructor() {
        super();
        this.state = {
            questions:[],
            active_card: null,
        }
        this._isMounted = false;
    }

    refreshData(){

        if (this.props.active_card){
            if (this.state.active_card !== this.props.active_card) {
                this.setState({questions: [], active_card: this.props.active_card})
            }
            getQuestions(this.props.active_card.id,(res)=>{
                this.setState({questions:res})
            })
        }
        console.log('card', this.state.questions)
    }

    componentDidMount() {
        this._isMounted = false;
        setInterval(()=>{
            this.refreshData()
        }, 1000)
    }

    componentWillUnmount() {
        this._isMounted = true;
    }

    check_comments(){
        if (this.props.active_card && (this.props.active_card.source == 'youtube' || this.props.active_card.source == 'vk')){
            return true
        }
        return false
    }
    check_platforms(){
        if (this.props.active_card && (this.props.active_card.source == 'youtube_videos' || this.props.active_card.source == 'vk_groups')){
            return true
        }
        return false
    }

    render() {
        return(
          <div>
              {this.props.active_card && this.check_comments() && this.state.questions.map((item)=>(
                <Post data={item}/>
              ))}
              {this.props.active_card && this.check_platforms() &&
                <div>
                    <Order card={this.props.active_card}/>
                </div>
              }
              {!this.props.active_card &&
                  <div className='spinner-item'>
                      <Spinner className='spinner-s' animation="grow" size="lg" variant="success" />
                  </div>
              }

          </div>
        )
    }
}