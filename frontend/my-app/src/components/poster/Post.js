import React, {Component} from "react";
import './Post.css'
import {Button, Form, FormControl, InputGroup} from "react-bootstrap";
import {addAnswer} from "../../backend/data";
import moment from 'moment'
export default class Post extends Component{
    constructor() {
        super();
        this.state={
            answer:'',
        }
        this.send = this.send.bind(this)
    }

    send(){
        let data = {
            question:this.props.data.id,
            text:this.state.answer,
        }
        addAnswer(data,(res)=>{
            console.log(res)
        })
    }

    get_question_link(){
        let params = JSON.parse(this.props.data.params)
        if (params.video_id){
            return  'https://www.youtube.com/watch?v='+params.video_id+'&lc='+params.id
        }
        else{
            return  'https://vk.com/'+params.screen_name+'?w=wall-'+params.group_id+'_'+params.post_id
        }

    }

    render() {
        return(
            <div className='post-item'>
                <div className='question'>
                    {this.props.data.text}
                </div>
                <div>
                    <a href={this.get_question_link()}> ссылка на коммент</a>
                </div>
                {!this.props.data.answer &&
                    <div className='input'>
                        <InputGroup className="mb-3">
                            <Form.Label>Ответ: </Form.Label>
                            <Form.Control value={this.state.value} onChange={(e)=>this.setState({answer:e.target.value})} as="textarea" rows="3" />
                        </InputGroup>
                    </div>
                }
                {this.props.data.answer &&
                    <div className='input'>
                        {this.props.data.answer.text}
                    </div>
                }
                <div>
                    <span>Дата: </span>
                    <span>{moment(this.props.data.post_time).format('DD MM YYYY hh:mm')}</span>
                </div>
                <div className='send'>
                    <Button variant="success" onClick={this.send}>Отправить</Button>
                </div>
            </div>
        )
    }
}