import React, {Component} from "react";
import {Navbar} from "react-bootstrap";

export default class Header extends Component{
    render() {
        return(
            <Navbar bg="dark" variant="dark">
                <Navbar.Brand href="#home">
                    <img
                        alt=""
                        src="https://bibleap.com/wp-content/uploads/9edf4439f658aa988f8b07821bx2-kartiny-i-panno-sotvorenie-adama-kopiya.jpg"
                        width="30"
                        height="30"

                        className="d-inline-block align-top"
                    />{' '}
                    Vigital-P
                </Navbar.Brand>
            </Navbar>
        )
    }
}