import React, {Component} from "react";

export default class Card extends Component{
    get_source(){
        if (this.props.data.source == 'vk_groups'){
            return 'ВКонтакте'
        }
        else if (this.props.data.source == 'youtube_videos'){
            return 'YouTube'
        }
    }
    render() {
        return(
            <div className='card-item'>
                <div>{this.props.data.title}</div>
                <div>{this.get_source()}</div>
            </div>
        )
    }
}