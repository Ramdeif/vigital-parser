import React, {Component} from "react";
import Card from "./Card";
import {Button, ListGroup} from "react-bootstrap";
import {getAuth, getTasks} from "../../backend/data";
import AddTaskItem from "./addTask";
import './Crads.css'

export default class Cards extends Component{
    constructor() {
        super();
        this.state = {
            cards: [],
            showAddTask:false,
            authlink:'',
        }
        this._isMounted = false;
    }
    refreshData(){
        getTasks((res)=>{
            if (this._isMounted){
                this.setState({cards:res})
            }
        })
        getAuth((res)=>{
            if (res && res.auth && res.auth.authorization_url && res.auth.authorization_url !== this.state.authlink){
                this.setState({authlink:res.auth.authorization_url})
                let win = window.open(res.auth.authorization_url, '_blank');
                if (win){win.focus()}
            }
        })

    }

    componentDidMount() {
        this._isMounted = true;
        setInterval(()=>{
            this.refreshData()
        }, 1000)
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    sandId(c){
        this.props.change(c)
    }
    render() {
        let modalClose = () => this.setState({ showAddTask: false });
        return(
            <div className='cards'>
                <div className='add-task'>
                    <Button variant="warning" onClick={()=>{this.setState({showAddTask:true})}}>Добавить задание</Button>
                </div>
                <ListGroup defaultActiveKey="#link1">
                    {this.state.cards.map((card, i)=>{
                        //console.log(card)
                        return(
                            <ListGroup.Item onClick={()=>{this.sandId(card)}} href={'#link'+(i+1)}>
                                <Card data={card}/>
                            </ListGroup.Item>
                        )
                    })}
                </ListGroup>
                <AddTaskItem
                    show={this.state.showAddTask}
                    onHide={modalClose}
                />
            </div>
        )
    }
}