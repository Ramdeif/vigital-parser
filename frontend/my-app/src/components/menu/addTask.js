import React, {Component} from "react";
import {addTasks} from "../../backend/data";
import _ from 'lodash';
import {Button, Col, Form, Modal} from "react-bootstrap";
import DatePicker from "react-datepicker";
import moment from 'moment'

import "react-datepicker/dist/react-datepicker.css";

export default class AddTaskItem extends Component{
    constructor() {
        super();
        this.state={
            title:'',
            semantic:'',
            area:'ВКонтакте площадки',
            count:0,
            video_count:0,
            groups_count:0,
            posts_count:0,
            startDate: new Date(),
        }
        this.add = this.add.bind(this)
        this.handleChange = this.handleChange.bind(this);
    }

    changeType(t){
        if (t === 'ВКонтакте площадки'){return 'vk_groups'}
        else if (t === 'ВКонтакте комментарии'){return 'vk'}
        else if (t === 'YouTube площадки'){return 'youtube_videos'}
        else if (t === 'YouTube комментарии'){return 'youtube'}
        console.log('t', t)
    }

    add(){

        let params = JSON.stringify({
            query:this.state.semantic,
            count:+this.state.count > 49 ? 50:+this.state.count,
            fromTime:moment.utc(this.state.startDate).toISOString(),
            video_count: +this.state.video_count > 49 ? 50:+this.state.video_count,
            groups_count: +this.state.groups_count > 49 ? 50:+this.state.groups_count,
            posts_count: +this.state.posts_count > 49 ? 50:+this.state.posts_count,
        })
        let data ={
            title:this.state.title,
            source: this.changeType(this.state.area),
            params: params,
        }
        console.log('data', data)
        console.log('source', data.source)
        addTasks(data,(res)=>{
            console.log('log', res)
        })

        this.props.onHide()
    }

    handleChange(date) {
        this.setState({
            startDate: date
        });

    }

    render() {
        return(
            <Modal
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                {...this.props}
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Добавления Задания
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group controlId="Title">
                            <Form.Label>Название задачи</Form.Label>
                            <Form.Control type="text" value={this.state.title} onChange={(e)=>this.setState({title:e.target.value})}/>
                        </Form.Group>

                        <Form.Group controlId="exampleForm.ControlTextarea1">
                            <Form.Label>Фразы для поиска</Form.Label>
                            <Form.Control value={this.state.semantic} onChange={(e)=>this.setState({semantic:e.target.value})} as="textarea" rows="3" />
                        </Form.Group>

                        <Form.Row>
                            <Form.Group as={Col} controlId="formGridState">
                                <Form.Label>Выбор площадки</Form.Label>
                                <Form.Control value={this.state.area} onChange={(e)=>this.setState({area:e.target.value})} as="select" >
                                    <option>YouTube комментарии</option>
                                    <option>ВКонтакте площадки</option>
                                    <option>ВКонтакте комментарии</option>
                                    <option>YouTube площадки</option>
                                </Form.Control>
                            </Form.Group>
                            {(this.state.area === 'ВКонтакте площадки' || this.state.area === 'YouTube площадки') &&
                                <div>
                                    <Form.Group controlId="Title">
                                        <Form.Label>Колличество</Form.Label>
                                        <Form.Control type="text" value={this.state.count} onChange={(e)=>this.setState({count:e.target.value})}/>
                                    </Form.Group>
                                </div>
                            }
                        </Form.Row>
                        {this.state.area === 'YouTube комментарии' &&
                            <div>
                                <div>Дата</div>
                                <DatePicker
                                    selected={this.state.startDate}
                                    onChange={this.handleChange}
                                    timeInputLabel="Time:"
                                    dateFormat="MM/dd/yyyy h:mm aa"
                                    showTimeInput
                                />
                                <Form.Group controlId="Title">
                                    <Form.Label>Колличество видео</Form.Label>
                                    <Form.Control type="text" value={this.state.video_count} onChange={(e)=>this.setState({video_count:e.target.value})}/>
                                </Form.Group>
                            </div>
                        }

                        {this.state.area === 'ВКонтакте комментарии' &&
                            <div>
                                <div>Дата</div>
                                <DatePicker
                                    selected={this.state.startDate}
                                    onChange={this.handleChange}
                                    timeInputLabel="Time:"
                                    dateFormat="MM/dd/yyyy h:mm aa"
                                    showTimeInput
                                />
                                <Form.Group controlId="Title">
                                    <Form.Label>Колличество групп</Form.Label>
                                    <Form.Control type="text" value={this.state.groups_count} onChange={(e)=>this.setState({groups_count:e.target.value})}/>
                                </Form.Group>
                                <Form.Group controlId='video'>
                                    <Form.Label>Колличество постов</Form.Label>
                                    <Form.Control type="text" value={this.state.posts_count} onChange={(e)=>this.setState({posts_count:e.target.value})}/>
                                </Form.Group>
                            </div>
                        }

                        <Button variant="primary" onClick={this.add}>
                            Добавить
                        </Button>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.props.onHide}>Close</Button>
                </Modal.Footer>
            </Modal>
        )
    }
}