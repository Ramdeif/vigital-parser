export function getTasks(callback) {
    fetch('/api/search_tasks/', {
        // headers: {
        //     'Accept': 'application/json',
        //     'Content-Type': 'application/json'
        // },
    })
        .then(resp => {
            //console.log(resp)
            return  resp.json();
        })
        .then(res => {
            //console.log(res)
            callback(res)
        })
}

export function getPlaces(i, callback) {
    fetch('/api/places/?task_id='+i, {

    })
        .then(resp => {
            //console.log(resp)
            return  resp.json();
        })
        .then(res => {
            console.log(res)
            callback(res)
        })
}

export function getQuestions(i, callback) {
    fetch('/api/questions/?task_id='+i, {

    })
        .then(resp => {
            //console.log(resp)
            return  resp.json();
        })
        .then(res => {
            console.log(res)
            callback(res)
        })
}

export function getAuth(callback) {
    fetch('/api/youtube_auth/', {

    })
        .then(resp => {
            //console.log(resp)
            return  resp.json();
        })
        .then(res => {
            console.log(res)
            callback(res)
        })
}

export function addTasks(data, callback) {
    fetch('/api/search_tasks/', {
        method:'POST',
        body:JSON.stringify(data),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    })
        .then(resp => {
            //console.log(resp)
            return  resp.json();
        })
        .then(res => {
            //console.log(res)
            callback(res)
        })
}

export function addAnswer(data, callback) {
    fetch('/api/answers/', {
        method:'POST',
        body:JSON.stringify(data),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    })
        .then(resp => {
            //console.log(resp)
            return  resp.json();
        })
        .then(res => {
            //console.log(res)
            callback(res)
        })
}