import React, {Component} from 'react';
import './App.css';
import {Container, Row, Col} from "react-bootstrap";
import Cards from "./components/menu/Cards";
import Header from "./components/header/header";
import Poster from "./components/poster/Poster";

export default class App extends Component {
    constructor() {
        super();
        this.state={
            activ_task:null,
        }
        this.changeActiveTask = this.changeActiveTask.bind(this)
    }

    changeActiveTask(card){
        this.setState({activ_task:card})
    }
    render() {
        return (
            <div className="App">
                <div className='header'>
                    <Header/>
                </div>
                <Container>
                    <Row>
                        <Col sm={3}>
                            <Cards change={this.changeActiveTask}/>
                        </Col>
                        <Col sm={9}>
                            <Poster active_card={this.state.activ_task}/>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}
