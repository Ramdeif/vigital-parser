from django.urls import path

from api import views

urlpatterns = [
    path('search_tasks/', views.SearchTaskList.as_view()),
    path('search_tasks/<int:pk>/', views.SearchTaskDetail.as_view()),

    path('questions/', views.QuestionList.as_view()),
    path('questions/<int:pk>', views.QuestionDetail.as_view()),

    path('answers/', views.AnswerCreate.as_view()),

    path('places/', views.PlaceList.as_view()),
    path('places/<int:pk>', views.PlaceDetail.as_view()),

    path('youtube_auth/', views.YoutubeAuthView.as_view()),
    path('youtube_oauth_callback/', views.YoutubeOauthCallback.as_view(),
         name='youtube_oauth_callback'),
]
