from django.shortcuts import render
from rest_framework import views, generics, exceptions, status
from rest_framework.response import Response

from api.models import SearchTask, Question, Answer, Place, YoutubeAuth
from api.serializers import SearchTaskSerializer, QuestionSerializer, AnswerSerializer, PlaceSerializer, \
    YoutubeAuthSerializer
from parsers import youtube_comments


class SearchTaskList(generics.ListCreateAPIView):
    serializer_class = SearchTaskSerializer
    queryset = SearchTask.objects.all()


class SearchTaskDetail(generics.RetrieveDestroyAPIView):
    serializer_class = SearchTaskSerializer
    queryset = SearchTask.objects.all()


class QuestionList(generics.ListCreateAPIView):
    serializer_class = QuestionSerializer

    def get_queryset(self):
        queryset = Question.objects.all()

        task_id = self.request.query_params.get('task_id')
        if task_id is not None:
            if not SearchTask.objects.filter(id=task_id).exists():
                raise exceptions.NotFound('Task not found')
            queryset = queryset.filter(search_task=task_id)

        return queryset


class QuestionDetail(generics.RetrieveAPIView):
    serializer_class = QuestionSerializer
    queryset = Question.objects.all()


class AnswerCreate(generics.CreateAPIView):
    serializer_class = AnswerSerializer
    queryset = Answer.objects.all()


class PlaceList(generics.ListCreateAPIView):
    serializer_class = PlaceSerializer

    def get_queryset(self):
        queryset = Place.objects.all()

        task_id = self.request.query_params.get('task_id')
        if task_id is not None:
            if not SearchTask.objects.filter(id=task_id).exists():
                raise exceptions.NotFound('Task not found')
            queryset = queryset.filter(search_task=task_id)

        return queryset


class PlaceDetail(generics.RetrieveAPIView):
    serializer_class = PlaceSerializer
    queryset = Place.objects.all()


class YoutubeAuthView(views.APIView):
    def get(self, request):
        auth = YoutubeAuth.objects.first()
        # ser = YoutubeAuthSerializer(auth)
        if auth is not None:
            url = auth.authorization_url
        else:
            url = None
        return Response({'auth': {
            'authorization_url': url,
        }})


class YoutubeOauthCallback(views.APIView):
    def get(self, request):
        youtube_comments.on_youtube_callback(request)
        return Response(status=status.HTTP_204_NO_CONTENT,
                        content_type=None)
