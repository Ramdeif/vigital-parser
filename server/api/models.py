from django.db import models


class SearchTask(models.Model):
    class Source:
        YOUTUBE = 'youtube'
        VK = 'vk'
        YOUTUBE_VIDEOS = 'youtube_videos'
        VK_GROUPS = 'vk_groups'

        choices = (
            (YOUTUBE, YOUTUBE),
            (VK, VK),
            (YOUTUBE_VIDEOS,) * 2,
            (VK_GROUPS,) * 2,
        )
        max_len = 20



    title = models.CharField(default='Выгрузка',
                             max_length=100)
    create_time = models.DateTimeField(auto_now_add=True)
    source = models.CharField(choices=Source.choices,
                              max_length=Source.max_len)
    is_completed = models.BooleanField(default=False)

    # JSON serialized search params
    params = models.TextField()

    def __str__(self):
        return 'task ' + str(self.id) + ' [' + self.title + ']'


class Question(models.Model):
    search_task = models.ForeignKey(SearchTask,
                                    related_name='questions',
                                    on_delete=models.CASCADE)
    text = models.TextField()
    post_time = models.DateTimeField()
    params = models.TextField()
    # answer = models.OneToOneField(Answer,
    #                               related_name='question',
    #                               default=None,
    #                               null=True,
    #                               on_delete=models.SET_DEFAULT)


class Answer(models.Model):
    text = models.TextField()
    create_time = models.DateTimeField(auto_now_add=True)
    params = models.TextField()
    is_sent = models.BooleanField(default=False)
    sending_failed = models.BooleanField(default=False)
    question = models.OneToOneField(Question, on_delete=models.CASCADE,
                                    related_name='answer')


class Place(models.Model):
    search_task = models.ForeignKey(SearchTask,
                                    related_name='places',
                                    on_delete=models.CASCADE)
    title = models.TextField()
    params = models.TextField()


class YoutubeAuth(models.Model):
    authorization_url = models.TextField(null=True)
    state = models.TextField()
    credentials = models.TextField(null=True)
