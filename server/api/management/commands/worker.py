import json
import os
import random
import re
import sys
import time
from threading import Thread

from django.core.management import BaseCommand

from api.models import SearchTask, Answer, Question, Place
from parsers import youtube_comments, vk_comments
from parsers.comment_filter import get_filter_func
from parsers.vk_comments import get_vk_auth


def fetch_questions_for_task(task):
    print('start fetching for task', task)
    params = json.loads(task.params)
    query = params['query']
    phrases = re.split('\.|,|;|\n', query)
    phrases = [item.strip() for item in phrases]

    video_count = params.get('video_count', 50)
    groups_count = params.get('groups_count', 50)
    posts_count = params.get('posts_count', 10)
    min_date = params.get('fromTime')

    if task.source == SearchTask.Source.YOUTUBE:
        # youtube = youtube_comments.get_authenticated_service()
        youtube = youtube_comments.get_youtube_auth()
        if youtube is None:
            return

        filter_func = get_filter_func(query, 45, ['?'], min_date)
        for phrase in phrases:
            comments = youtube_comments.get_comments_search(youtube, phrase, filter_func, video_count)

        print('found comments:', len(comments))

        for item in comments:
            Question.objects.create(search_task=task,
                                    text=item['text'],
                                    post_time=item['date'],
                                    params=json.dumps(item['source']))

    elif task.source == SearchTask.Source.VK:
        vk = get_vk_auth()
        filter_func = get_filter_func(query, 45, ['?'], min_date)
        comments = vk_comments.get_comments_search(vk, phrases, filter_func, groups_count, posts_count)

        print('found comments:', len(comments))

        for item in comments:
            Question.objects.create(search_task=task,
                                    text=item['text'],
                                    post_time=item['date'],
                                    params=json.dumps(item['source']))

    elif task.source == SearchTask.Source.YOUTUBE_VIDEOS:
        # youtube = youtube_comments.get_authenticated_service()
        youtube = youtube_comments.get_youtube_auth()
        if youtube is None:
            return

        count = params['count']
        for phrase in phrases:
            videos = youtube_comments.get_video_search(youtube, phrase, count)
            for item in videos:
                Place.objects.create(search_task=task,
                                     params=json.dumps(item))

    elif task.source == SearchTask.Source.VK_GROUPS:
        vk = get_vk_auth()
        count = params['count']
        for phrase in phrases:
            groups = vk_comments.get_groups_search(vk, phrase, count)
            for item in groups:
                Place.objects.create(search_task=task,
                                    params=json.dumps(item))

    task.is_completed = True
    task.save()

def fetch_questions():
    while True:
        tasks = SearchTask.objects.filter(is_completed=False)
        if tasks.exists():
            for task in tasks:


                # # Заглушка
                # time.sleep(10)
                # for i in range(10):
                #     Question.objects.create(search_task=task,
                #                             text='Пример вопроса ' + str(random.randint(1, 10**6)),
                #                             params='{}')
                #     time.sleep(0.1)

                #try:
                fetch_questions_for_task(task)
                #except Exception as e:
                #    print(e)

                time.sleep(1)
        else:
            time.sleep(1)


def send_answer(answer):
    if answer.question.search_task.source == SearchTask.Source.YOUTUBE:
        params = json.loads(answer.question.params)
        # youtube = youtube_comments.get_authenticated_service()
        youtube = youtube_comments.get_youtube_auth()
        if youtube is None:
            return
        res = youtube_comments.send_comment_answer(youtube, params['id'], answer.text)
        answer.params = json.dumps(res)
        answer.save()

    elif answer.question.search_task.source == SearchTask.Source.VK:
        params = json.loads(answer.question.params)
        vk = get_vk_auth()
        res = vk_comments.send_comment_answer(vk, answer.text,
                                              params['group_id'],
                                              params['post_id'],
                                              params['id'])
        answer.params = json.dumps(res)
        answer.save()


def send_answers():
    while True:
        answers = Answer.objects.filter(is_sent=False,
                                        sending_failed=False)
        if answers.exists():
            for answer in answers:

                try:
                    send_answer(answer)

                    answer.is_sent = True
                    answer.save()
                except Exception as e:
                    answer.sending_failed = True
                    answer.save()
                time.sleep(1)
        time.sleep(1)


class Command(BaseCommand):
    def handle(self, *args, **options):
        print('Start parser worker process')
        Thread(target=fetch_questions).start()
        Thread(target=send_answers).start()
