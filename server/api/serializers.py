import json
from json import JSONDecodeError

from rest_framework import serializers, validators

from api.models import SearchTask, Question, Answer, Place, YoutubeAuth


def validate_json(value):
    try:
        obj = json.loads(value)
    except JSONDecodeError:
        raise serializers.ValidationError('Parsing error')

    if not isinstance(obj, dict):
        raise serializers.ValidationError('Root must be an object')

    return obj


class SearchTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = SearchTask
        fields = '__all__'
        read_only_fields = ['is_completed']

    def validate_params(self, value):
        obj = validate_json(value)
        if 'query' not in obj:
            raise serializers.ValidationError('Params must include query field')
        return value


class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = '__all__'
        read_only_fields = ['is_sent', 'sending_failed', 'params']

    def validate_params(self, value):
        validate_json(value)
        return value


class QuestionSerializer(serializers.ModelSerializer):
    answer = AnswerSerializer(read_only=True)

    class Meta:
        model = Question
        fields = '__all__'

    def validate_params(self, value):
        validate_json(value)
        return value


class PlaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Place
        fields = '__all__'

    def validate_params(self, value):
        validate_json(value)
        return value


class YoutubeAuthSerializer(serializers.Serializer):
    class Meta:
        model = YoutubeAuth
        fields = ['authorization_url']
