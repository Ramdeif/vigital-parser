import base64
import pickle
from datetime import datetime, timezone

import google_auth_oauthlib
import xlwt
from django.urls import reverse
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from google_auth_oauthlib.flow import InstalledAppFlow

# from oauth2client.client import flow_from_clientsecrets
# from oauth2client.file import Storage
# from oauth2client.tools import argparser, run_flow


# The CLIENT_SECRETS_FILE variable specifies the name of a file that contains
# the OAuth 2.0 information for this application, including its client_id and
# client_secret. You can acquire an OAuth 2.0 client ID and client secret from
# the {{ Google Cloud Console }} at
# {{ https://cloud.google.com/console }}.
# Please ensure that you have enabled the YouTube Data API for your project.
# For more information about using OAuth2 to access the YouTube Data API, see:
#   https://developers.google.com/youtube/v3/guides/authentication
# For more information about the client_secrets.json file format, see:
#   https://developers.google.com/api-client-library/python/guide/aaa_client_secrets
from api.models import YoutubeAuth
from parsers.comment_filter import get_filter_func
from server import settings

CLIENT_SECRETS_FILE = 'parsers/youtube_creds/client_secret_1.json'

CRED_STORE_FILE = 'parsers/youtube_creds/creds_cache'

# This OAuth 2.0 access scope allows for full read/write access to the
# authenticated user's account.
SCOPES = ['https://www.googleapis.com/auth/youtube.force-ssl']
API_SERVICE_NAME = 'youtube'
API_VERSION = 'v3'

# def cache_func(param_nums):
#     def inner(func):
#         def wrapper(*args, **kwargs):
#


# Authorize the request and store authorization credentials.
def get_authenticated_service():
    flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRETS_FILE, SCOPES)

    try:
        with open(CRED_STORE_FILE, 'rb') as f:
            creds = pickle.load(f)
    except:
        creds = None

    if not creds or not creds.valid:
        creds = flow.run_local_server()
        with open(CRED_STORE_FILE, 'wb') as f:
            pickle.dump(creds, f)

    # creds = flow.run_local_server()

    return build(API_SERVICE_NAME, API_VERSION, credentials=creds)

    # flow = flow_from_clientsecrets(CLIENT_SECRETS_FILE, scope=SCOPES)
    #
    # storage = Storage("sore-oauth2.json")
    # credentials = storage.get()
    #
    # if credentials is None or credentials.invalid:
    #     credentials = run_flow(flow, storage, args)


def request_youtube_auth():
    flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
        CLIENT_SECRETS_FILE, scopes=SCOPES)

    # The URI created here must exactly match one of the authorized redirect URIs
    # for the OAuth 2.0 client, which you configured in the API Console. If this
    # value doesn't match an authorized URI, you will get a 'redirect_uri_mismatch'
    # error.
    flow.redirect_uri = settings.DOMAIN + reverse('youtube_oauth_callback')

    authorization_url, state = flow.authorization_url(
        # Enable offline access so that you can refresh an access token without
        # re-prompting the user for permission. Recommended for web server apps.
        # access_type='offline',
        # Enable incremental authorization. Recommended as a best practice.
        include_granted_scopes='true')

    YoutubeAuth.objects.create(credentials=None,
                               authorization_url=authorization_url,
                               state=state)


def get_youtube_auth():

    auth = YoutubeAuth.objects.first()
    if auth is None:
        request_youtube_auth()
        return None

    creds = auth.credentials
    if creds is not None:
        creds = pickle.loads(base64.b64decode(creds.encode()))
        if creds.valid:
            return build(API_SERVICE_NAME, API_VERSION, credentials=creds)
        else:
            auth.delete()
            request_youtube_auth()
            return None
    else:
        return None


def on_youtube_callback(request):
    code = request.query_params['code']
    state = request.query_params['state']

    flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
        CLIENT_SECRETS_FILE, scopes=SCOPES, state=state)

    flow.redirect_uri = settings.DOMAIN + reverse('youtube_oauth_callback')
    flow.fetch_token(code=code, state=state)
    creds = flow.credentials
    auth = YoutubeAuth.objects.first()
    if creds and creds.valid:
        auth.credentials = base64.b64encode(pickle.dumps(creds)).decode()
        auth.authorization_url = None
        auth.save()
    else:
        auth.delete()


def get_comments(youtube, video, filter_func):
    video_id = video['id']['videoId']
    channel_id = video['snippet']['channelId']

    comments = []
    next_page_token = None

    while True:

        res = youtube.commentThreads().list(
            part='snippet',
            textFormat="plainText",
            maxResults=100,
            videoId=video_id,
            pageToken=next_page_token,
        ).execute()

        # print('comments count', len(res['items']))
        for item in res['items']:
            try:
                entry = {
                    'text': item['snippet']['topLevelComment']['snippet']['textDisplay'],
                    'date': datetime.fromisoformat(item['snippet']['topLevelComment']['snippet']['publishedAt'][:-1]).astimezone(timezone.utc).isoformat(),
                    'type': 'youtube',
                    'source': {
                        'id': item['id'],
                        'video_id': item['snippet']['videoId'],
                        'author_id': item['snippet']['topLevelComment']['snippet']['authorChannelId']['value'],
                        'channel_id': channel_id,
                    }
                }
                if filter_func(entry):
                    comments.append(entry)
            except KeyError as e:
                print("ERROR", e)

        # page_token =
        if 'nextPageToken' in res:
            next_page_token = res['nextPageToken']
        else:
            break

    return comments


def get_comments_search(youtube, query, filter_func, count=50):
    comments = []

    count = min(count, 50)
    videos = youtube.search().list(
        part='snippet',
        maxResults=count,
        type='video',
        relevanceLanguage='ru',
        q=query,
    ).execute()

    for video in videos['items']:
        try:
            res = get_comments(youtube, video, filter_func)
            comments += res
        except HttpError:
            pass

    return comments


def get_channels_search(youtube, query):
    channels = []

    res = youtube.search().list(
        part='snippet',
        maxResults=50,
        type='channel',
        relevanceLanguage='ru',
        q=query,
    ).execute()

    for item in res['items']:
        entry = {
            'id': item['snippet']['channelId'],
            'title': item['snippet']['title'],
        }

        channels.append(entry)

    return channels


def send_comment_answer(youtube, comment_id, text):
    res = youtube.comments().insert(
        part='snippet',
        body=dict(
            snippet=dict(
                parentId=comment_id,
                textOriginal=text,
            )
        ),
    ).execute()

    return {
        'id': res['id'],
        'parent_id': res['snippet']['parentId'],
    }


def get_video_search(youtube, query, max_results):
    res = []

    videos = youtube.search().list(
        part='id',
        maxResults=max_results,
        type='video',
        relevanceLanguage='ru',
        q=query,
    ).execute()

    ids = ','.join([video['id']['videoId'] for video in videos['items']])

    details = youtube.videos().list(
        part='snippet,statistics',
        id=ids,
    ).execute()

    for item in details['items']:
        try:
            entry = {
                'video_id': item['id'],
                'title': item['snippet']['title'],
                'channel_id': item['snippet']['channelId'],
                'data': item['snippet']['publishedAt'],
                'stats': {
                    'views': item['statistics']['viewCount'],
                    'likes': item['statistics']['likeCount'],
                    'dislikes': item['statistics']['dislikeCount'],
                    'favorites': item['statistics']['favoriteCount'],
                    'comments': item['statistics']['commentCount'],
                }
            }
            res.append(entry)
        except Exception as e:
            print(e)

    return res



def get_channel_url(id):
    return 'https://youtube.com/channel/' + id

def get_video_url(id):
    return 'https://youtube.com/watch?v=' + id

if __name__ == '__main__':
    youtube = get_authenticated_service()

    query = 'тренажерный зал фитнес тренировка похудеть'

    res = get_video_search(youtube, query, 10)
    print(res)

    # filter_func = get_filter_func(query, 45, ['?'])
    # comments = get_comments_search(youtube, query, filter_func)
    # print(*comments, sep='\n')
    # print('total comments', len(comments))

    # xl = xlwt.Workbook()
    # sheet = xl.add_sheet('Комментарии')
    # sheet.write(0,0,'Youtube Канал')
    # sheet.write(0, 1, 'Видео')
    # sheet.write(0, 2, 'Комментарий')
    # for i, item in enumerate(comments):
    #     channel_url = get_channel_url(item['source']['channel_id'])
    #     video_url = get_video_url(item['source']['video_id'])
    #     text = item['text']
    #     sheet.write(i + 1, 0, channel_url)
    #     sheet.write(i + 1, 1, video_url)
    #     sheet.write(i + 1, 2, text)
    # xl.save('comments.xls')


    # channels = get_channels_search(youtube, query)
    # xl = xlwt.Workbook()
    # sheet = xl.add_sheet('Каналы')
    # sheet.write(0,0,'Youtube Канал')
    # sheet.write(0, 1, 'Название')
    # for i, item in enumerate(channels):
    #     url = get_channel_url(item['id'])
    #     sheet.write(i + 1, 0, url)
    #     sheet.write(i + 1, 1, item['title'])
    # xl.save('channels.xls')


    # for item in comments:
    #     text = item['snippet']['topLevelComment']['snippet']['textDisplay']
    #     print(text)
    #     if 'replies' in item:
    #         for reply in item['replies']['comments']:
    #             text = reply['snippet']['textDisplay']
    #             print('\t', text)




