import vk_api

from datetime import datetime, timezone


def get_vk_auth():
    return auth_vk()


def get_groups_id(vk, word, count):

    res = vk.method('groups.search', {
        'q': word,
        'count': count,
    })
    res = [x['id'] for x in res['items']]
    return res


def send_comment_answer(vk, text, group_id, post_id, comment_id):
    vk.method('wall.createComment', {'owner_id': -group_id, 'post_id': post_id, 'reply_to_comment': comment_id, 'message':text})
    return {}


def get_comments_search(vk, phrases, filter_func, groups_count=50, posts_count=50):
    res_comments = []
    for word in phrases:
        groups_id = get_groups_id(vk, word , groups_count)
        for group_id in groups_id:
            try:
                gr = vk.method('groups.getById', {'group_id': group_id, 'fields': 'screen_name'})
                wall = vk.method('wall.get', {'owner_id': -group_id, 'count':posts_count})
                for post in wall['items']:
                    comments = vk.method('wall.getComments', {'owner_id': -group_id, 'post_id': post['id']})
                    if comments['count'] > 0:
                        comments_items = [comment for comment in comments['items'] if 'text' in comment]
                        for c in comments_items:
                            dt = datetime.utcfromtimestamp(c['date']).astimezone(timezone.utc).isoformat()
                            res = {
                                'text':c['text'],
                                'date':dt,
                                'type':'VK',
                                'source':{
                                        'id':c['id'],
                                        'from_id':c['from_id'],
                                        'post_id':c['post_id'],
                                        'owner_id':c['owner_id'],
                                        'screen_name': gr[0]['screen_name'],
                                        'group_id': group_id,
                                    }
                            }
                            if filter_func(res):
                                res_comments.append(res)
            except:
                pass
    return res_comments


# Функция для Федука(возвращает список групп)
def get_groups_search(vk, query, count):
    res = []
    try:
        groups_id = get_groups_id(vk, query, count)

        for group_id in groups_id:
            gr = vk.method('groups.getById', {'group_id':group_id, 'fields':'members_count, screen_name, name'})
            wal = vk.method('wall.get', {'owner_id':-group_id, 'count':10})
            like = sum([x['likes']['count'] for x in wal['items']])
            rep = sum([x['reposts']['count'] for x in wal['items']])
            com = sum([x['comments']['count'] for x in wal['items']])
            res.append(
                {
                    'id': gr[0]['screen_name'],
                    'title':gr[0]['name'],
                    'stats':{
                        'members_count':gr[0]['members_count'],
                        'likes':like,
                        'comments':com,
                        'rep': rep,
                    }
                }
            )
    except:
        pass
    return res


def auth_vk(login, password):
    vk = vk_api.VkApi(login=login, password=password)
    vk.auth()
    return vk


if __name__ == '__main__':
    vk = get_vk_auth()
    res_comments = get_groups_search(vk, ['похудение'], 10)
    print(res_comments)
