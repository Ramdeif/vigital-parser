import os
from threading import Thread
import waitress
from django.core.management.commands import migrate

from server.wsgi import application

# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "server.settings")

# from django.core.management import call_command
# from django.core.wsgi import get_wsgi_application

import manage
import api
import server
import api.urls
import rest_framework.templates.rest_framework
from rest_framework.templates import *
from django.views.templates import *
# import django.template.load
# application = get_wsgi_application()

def run_worker():
    # call_command('worker')
    from api.management.commands.worker import Command
    Command().handle()


# Thread(target=run_worker).start()

# call_command('runserver',  '127.0.0.1:8000')
waitress.serve(application, port='8000')
