import json
import time

import requests
import xlwt

url = 'http://api.bukvarix.com/v1/keywords/'


def get_word_stat(query):
    res = requests.get(url, params={
        'q': query, 'api_key': 'free',
        'format': 'json',
        'num': 1,
    })
    print(res.text)
    res = json.loads(res.text)
    return res['data'][0][4]


# k = get_word_stat('фитнес')
# print(k)

main_words = '''фитнес
тренажерный зал
похудеть'''.split('\n')

regions = '''Юго-Западная
Крымская
Чертановская
Марьина Роща
Беляево
Проезд Дежнева
Рязанский проспект
Одинцово
Крылатское
Селигерская
Башня
Федерация
Щукинская'''.split('\n')

print(regions)

xl = xlwt.Workbook()
sheet = xl.add_sheet('sheet-1')
# sheet.write(0,1, 'hello')


for i in range(len(regions)):
    sheet.write(0, 2*i, 'Запрос')
    sheet.write(0, 2*i+1, 'Частота')

for i in range(len(regions)):
    for j in range(len(main_words)):
        row = j + 1
        col = 2 * i
        query = main_words[j] + ' ' + regions[i]
        sheet.write(row, col, query)
        try:
            count = get_word_stat(query)
            sheet.write(row, col + 1, count)
        except:
            pass
        time.sleep(0.5)

xl.save('book-1.xls')