from fuzzywuzzy import fuzz


def get_filter_func(query, min_ratio=30, must_include=[]):

    def filter_comment(comment):
        text = comment['text']

        for s in must_include:
            if s not in text:
                return False

        ratio = fuzz.token_set_ratio(text, query)
        if ratio < min_ratio:
            return False

        return True

    return filter_comment
