import vk_api

from comment_filter import get_filter_func
from config import POSTS_COUNT, GROPS_COUNT
from datetime import datetime, timezone


def get_vk_auth():
    return auth_vk(,)


def get_groups_id(word):
    res = vk.method('groups.search', {
        'q': word,
        'count': GROPS_COUNT,
        'sort':0,
    })
    res = [x['id'] for x in res['items']]
    return res





def get_comments_search(vk, query, filter_func):
    res_comments = []
    groups_id = get_groups_id(word=query)
    for group_id in groups_id:
        gr = vk.method('groups.getById', {'group_id':group_id, 'fields':'members_count, screen_name, name'})
        res_comments.append(gr)
    return res_comments


def auth_vk(login, password):
    vk = vk_api.VkApi(login=login, password=password)
    vk.auth()
    return vk

def get_group_url(z):
    return 'https://vk.com/'+z

def write_res(res_comments, a):
    global xl
    sheet = xl.add_sheet(a)
    i = 0
    for x in res_comments:
        url = get_group_url(x[0]['screen_name'])
        sheet.write(i, 0, url)
        sheet.write(i, 1, x[0]['name'])
        sheet.write(i, 2, x[0]['members_count'])
        i += 1


# Функция для Федука(возвращает список групп)
def get_groups_search(vk, query):
    res = []
    for w in query:
        groups_id = get_groups_id(word=w)
        for group_id in groups_id:
            gr = vk.method('groups.getById', {'group_id':group_id, 'fields':'members_count, screen_name, name'})
            wal = vk.method('wall.get', {'owner_id':group_id, 'count':10})
            like = [x['likes']['count'] for x in wal['items']]
            com = [x['comments']['count'] for x in wal['items']]
            res.append(
                {
                    'id': group_id,
                    'title':gr[0]['name'],
                    'statistic':{
                        'url': get_group_url(group_id),
                        'members_count':gr[0]['members_count'],
                        'likes':wal['items'],
                        'comments':1,
                    }
                }
            )
        return res


vk = get_vk_auth()
array = [
    'Похудеть',
    'Женский фитнес',
    'тренировки для начинающих',
    'Набрать мышечную массу',
    'тренировка пресса',
    'тренировка ягодиц',
    'тренировка рук',
    'тренировка живота',
    'тренировка ног',
    'лечебная физкультура',
]

res = get_groups_search(vk, array)
