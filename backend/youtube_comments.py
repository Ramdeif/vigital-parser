import pickle
from datetime import datetime, timezone

import xlwt
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from google_auth_oauthlib.flow import InstalledAppFlow

# from oauth2client.client import flow_from_clientsecrets
# from oauth2client.file import Storage
# from oauth2client.tools import argparser, run_flow


# The CLIENT_SECRETS_FILE variable specifies the name of a file that contains
# the OAuth 2.0 information for this application, including its client_id and
# client_secret. You can acquire an OAuth 2.0 client ID and client secret from
# the {{ Google Cloud Console }} at
# {{ https://cloud.google.com/console }}.
# Please ensure that you have enabled the YouTube Data API for your project.
# For more information about using OAuth2 to access the YouTube Data API, see:
#   https://developers.google.com/youtube/v3/guides/authentication
# For more information about the client_secrets.json file format, see:
#   https://developers.google.com/api-client-library/python/guide/aaa_client_secrets
from backend.comment_filter import get_filter_func

CLIENT_SECRETS_FILE = 'client_secret12.json'

CRED_STORE_FILE = 'creds'

# This OAuth 2.0 access scope allows for full read/write access to the
# authenticated user's account.
SCOPES = ['https://www.googleapis.com/auth/youtube.force-ssl']
API_SERVICE_NAME = 'youtube'
API_VERSION = 'v3'

# def cache_func(param_nums):
#     def inner(func):
#         def wrapper(*args, **kwargs):
#


# Authorize the request and store authorization credentials.
def get_authenticated_service():
    flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRETS_FILE, SCOPES)

    try:
        with open(CRED_STORE_FILE, 'rb') as f:
            creds = pickle.load(f)
    except:
        creds = None

    if not creds or not creds.valid:
        creds = flow.run_local_server()
        with open(CRED_STORE_FILE, 'wb') as f:
            pickle.dump(creds, f)

    # creds = flow.run_local_server()

    return build(API_SERVICE_NAME, API_VERSION, credentials=creds)

    # flow = flow_from_clientsecrets(CLIENT_SECRETS_FILE, scope=SCOPES)
    #
    # storage = Storage("sore-oauth2.json")
    # credentials = storage.get()
    #
    # if credentials is None or credentials.invalid:
    #     credentials = run_flow(flow, storage, args)

def get_comments_list(youtube, video, filter_func):
    video_id = video

    comments = []
    next_page_token = None

    while True:

        res = youtube.commentThreads().list(
            part='snippet',
            textFormat="plainText",
            maxResults=10,
            videoId=video_id,
            pageToken=next_page_token,
        ).execute()

        # print('comments count', len(res['items']))
        for item in res['items']:
            try:
                entry = {
                    'text': item['snippet']['topLevelComment']['snippet']['textDisplay'],
                    'date': item['snippet']['topLevelComment']['snippet']['publishedAt'],
                    'type': 'youtube',
                    'source': {
                        'id': item['id'],
                        'video_id': item['snippet']['videoId'],
                        'author_id': item['snippet']['topLevelComment']['snippet']['authorChannelId']['value'],
                    }
                }
                if filter_func(entry):
                    comments.append(entry)
            except KeyError as e:
                print("ERROR", e)

        # page_token =
        if 'nextPageToken' in res:
            next_page_token = res['nextPageToken']
        else:
            break

    return comments


def get_comments(youtube, video, filter_func):
    video_id = video['id']['videoId']
    channel_id = video['snippet']['channelId']

    comments = []
    next_page_token = None

    while True:

        res = youtube.commentThreads().list(
            part='snippet',
            textFormat="plainText",
            maxResults=100,
            videoId=video_id,
            pageToken=next_page_token,
        ).execute()

        # print('comments count', len(res['items']))
        for item in res['items']:
            try:
                entry = {
                    'text': item['snippet']['topLevelComment']['snippet']['textDisplay'],
                    'date': item['snippet']['topLevelComment']['snippet']['publishedAt'],
                    'type': 'youtube',
                    'source': {
                        'id': item['id'],
                        'video_id': item['snippet']['videoId'],
                        'author_id': item['snippet']['topLevelComment']['snippet']['authorChannelId']['value'],
                        'channel_id': channel_id,
                    }
                }
                if filter_func(entry):
                    comments.append(entry)
            except KeyError as e:
                print("ERROR", e)

        # page_token =
        if 'nextPageToken' in res:
            next_page_token = res['nextPageToken']
        else:
            break

    return comments

def get_comments_list_arr(youtube, videous, filter_func):
    comments = []
    arr =videous
    for video in arr:
        try:
            res = get_comments_list(youtube, video, filter_func)
            comments += res
        except HttpError:
            pass

    return comments

def get_comments_search(youtube, query, filter_func):
    comments = []

    videos = youtube.search().list(
        part='snippet',
        maxResults=50,
        type='video',
        relevanceLanguage='ru',
        q=query,
    ).execute()

    for video in videos['items']:
        try:
            res = get_comments(youtube, video, filter_func)
            comments += res
        except HttpError:
            pass

    return comments

def get_videos(youtube, query):
    videos = youtube.search().list(
        part='snippet',
        maxResults=5,
        type='video',
        relevanceLanguage='ru',
        q=query,
    ).execute()
    return videos

def get_channels_search(youtube, query):
    channels = []

    res = youtube.search().list(
        part='snippet',
        maxResults=50,
        type='channel',
        relevanceLanguage='ru',
        q=query,
    ).execute()

    for item in res['items']:
        entry = {
            'id': item['snippet']['channelId'],
            'title': item['snippet']['title'],
        }

        channels.append(entry)

    return channels


def get_channel_url(id):
    return 'https://youtube.com/channel/' + id

def get_video_url(id):
    return 'https://youtube.com/watch?v=' + id

def get_comment_url(video_id, comment_id):
    return 'https://www.youtube.com/watch?v='+video_id+'&lc='+comment_id

if __name__ == '__main__':
    youtube = get_authenticated_service()
    # import xlrd, xlwt
    # rb = xlrd.open_workbook('1.xls', formatting_info=True)
    #
    # # выбираем активный лист
    # sheet = rb.sheet_by_index(0)
    # arr = []
    # # print(sheet.row_values(2)[1])
    # for i in range(1, sheet.nrows):
    #     z = sheet.row_values(i)[0]
    #     z = z[28:]
    #     arr.append(z)
    # print(arr)
    array = [
                'Похудеть',
                'Женский фитнес',
                'Фитнес для начинающих',
                'Набрать мышечную массу',
                'Пресс',
                'Ягодицы',
                'Руки',
                'Живот',
                'Ноги',
                'Восстановление после травм / лечебная физкультура',
             ]
    filter_query = 'похудеть накачаться тренировки здоровье'
    filter_func = get_filter_func(filter_query, 45, ['?'])
    j = 7
    for x in array[7:len(array)]:
        #comments = get_comments_list_arr(youtube, arr[:10], filter_func)
        comments = get_comments_search(youtube, x, filter_func)
        # print(*comments, sep='\n')
        # print('total comments', len(comments))
        xl = xlwt.Workbook()
        sheet = xl.add_sheet('Комментарии похудение')
        sheet.write(0, 0, 'Дата')
        sheet.write(0, 1, 'Текст')
        sheet.write(0, 2, 'Ссылка')
        i = 1
        for item in comments:
            sheet.write(i, 0, item['date'])
            sheet.write(i, 1, item['text'])
            sheet.write(i, 2, get_comment_url(item['source']['video_id'], item['source']['id']))
            i += 1
        xl.save('comments'+str(j)+'.xls')
        j+=1
        print(j)
    # channels = get_channels_search(youtube, query)
    # xl = xlwt.Workbook()
    # sheet = xl.add_sheet('Каналы')
    # sheet.write(0,0,'Youtube Канал')
    # sheet.write(0, 1, 'Название')
    # for i, item in enumerate(channels):
    #     url = get_channel_url(item['id'])
    #     sheet.write(i + 1, 0, url)
    #     sheet.write(i + 1, 1, item['title'])
    # xl.save('channels.xls')


    # for item in comments:
    #     text = item['snippet']['topLevelComment']['snippet']['textDisplay']
    #     print(text)
    #     if 'replies' in item:
    #         for reply in item['replies']['comments']:
    #             text = reply['snippet']['textDisplay']
    #             print('\t', text)




